package com.reporting.Service;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import java.io.FileOutputStream;
import java.util.List;
@Service
public class ServiceXls  {

    public void createxls(Model model, org.apache.poi.ss.usermodel.Workbook workbook) throws Exception {


        @SuppressWarnings("unchecked")
        List<String> fields = (List<String>) model.getAttribute("fields");

        @SuppressWarnings("unchecked")
        List<Object[]>  data = (List<Object[]> )model.getAttribute("data");
        // create excel xls sheet
        Sheet sheet = workbook.createSheet("User Detail");
        sheet.setDefaultColumnWidth(30);

        // create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.BLUE.index);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);

        // create header row
        Row header = sheet.createRow(0);
       for(int i=0;i<fields.size();i++)
       {
           header.createCell(i).setCellValue(fields.get(i).toString());
           header.getCell(i).setCellStyle(style);
       }
        int rowCount = 1;

        for (Object[] r : data)
        {
            if(r !=null)
            {
                Row userRow = sheet.createRow(rowCount++);

                for (int k = 0; k < fields.size(); k++) {
                    if (r[k] == null) {
                        r[k] = "";
                    }
                    userRow.createCell(k).setCellValue(r[k].toString());
             }
           }
        }
        for(int i = 0; i < fields.size(); i++) {
            sheet.autoSizeColumn(i);
        }
        FileOutputStream fileOut = new FileOutputStream("poi-generated-file.xlsx");
        workbook.write(fileOut);
        fileOut.close();

        // Closing the workbook
        workbook.close();

    }

}
