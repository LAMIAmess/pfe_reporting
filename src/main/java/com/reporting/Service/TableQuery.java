package com.reporting.Service;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TableQuery {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @PersistenceContext
    private EntityManager em;

    public List<String> getAllTables()
    {
        EntityManager session = entityManagerFactory.createEntityManager();
        List<String>TablesName=(List<String>) session.createNativeQuery("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public' ORDER BY table_name").getResultList();
        session.close();
        return TablesName;
    }


    public List<String>getAllFields(String tablename)
    {
        EntityManager session = entityManagerFactory.createEntityManager();
        List<String>FieldsName=(List<String>) session.createNativeQuery("SELECT COLUMN_NAME FROM   INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = ?")
                                        .setParameter(1,tablename)
                                        .getResultList();
        session.close();
        return FieldsName;
    }


    public String getTypeOfField(String tablename,String champs)
    {
        EntityManager session = entityManagerFactory.createEntityManager();
        String TypeOfField=session.createNativeQuery("SELECT  DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = ? and COLUMN_NAME=?")
                           .setParameter(1,tablename)
                           .setParameter(2,champs)
                           .getSingleResult().toString();
        session.close();
        return TypeOfField;

    }
}
