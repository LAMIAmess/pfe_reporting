package com.reporting.Service;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.reporting.Model.RelationShip;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Service
public class RelationShipQuery {

    @Autowired
    private EntityManagerFactory entityManagerFactory;

    @PersistenceContext
    private EntityManager em;
public List<RelationShip>  findRelationShip(String tablename) throws JsonProcessingException {
    EntityManager session = entityManagerFactory.createEntityManager();
    StringBuffer stringBuffer=new StringBuffer();


    stringBuffer.append("SELECT tc.table_name, kcu.column_name, ccu.table_name AS foreign_table_name, " )
                .append(" ccu.column_name AS foreign_column_name FROM information_schema.table_constraints AS tc " )
                .append(" JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name " )
                .append(" AND tc.table_schema = kcu.table_schema JOIN information_schema.constraint_column_usage AS ccu ")
                .append(" ON ccu.constraint_name = tc.constraint_name  AND ccu.table_schema = tc.table_schema " )
                .append(" WHERE tc.constraint_type = 'FOREIGN KEY' AND tc.table_name=?");

    List<Object[]> relationShipsForTable= session.createNativeQuery(stringBuffer.toString())
                                                  .setParameter(1,tablename)
                                                  .getResultList();
    session.close();
    List<RelationShip>relationShipList=new ArrayList<>();
    for (Object[] r : relationShipsForTable) {
        RelationShip relationShip=new RelationShip();
        relationShip.setTablename(r[0].toString());
        relationShip.setColumnname(r[1].toString());
        relationShip.setTablenameforeign(r[2].toString());
        relationShip.setColumnnameforeign(r[3].toString());
        relationShipList.add(relationShip);
    }
   return relationShipList;

}
public List<String>findTableRelation(String tablename)
{
    EntityManager session = entityManagerFactory.createEntityManager();
    StringBuffer stringBuffer=new StringBuffer();


    stringBuffer.append("SELECT ccu.table_name AS foreign_table_name FROM information_schema.table_constraints AS tc " )
            .append(" JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name " )
            .append(" AND tc.table_schema = kcu.table_schema JOIN information_schema.constraint_column_usage AS ccu ")
            .append(" ON ccu.constraint_name = tc.constraint_name  AND ccu.table_schema = tc.table_schema " )
            .append(" WHERE tc.constraint_type = 'FOREIGN KEY' AND tc.table_name=? ");
    List<String> relationTable= session.createNativeQuery(stringBuffer.toString())
                                        .setParameter(1,tablename)
                                        .getResultList();
    session.close();
    return relationTable;
}
public List<String>findAllTablesRelation(String tablename)
{
    EntityManager session = entityManagerFactory.createEntityManager();
    StringBuffer stringBuffer=new StringBuffer();
    stringBuffer.append("SELECT tc.table_name, kcu.column_name, ccu.table_name AS foreign_table_name,ccu.column_name AS foreign_column_name ")
                .append("  FROM information_schema.table_constraints AS tc" )
                .append(" JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name " )
                .append(" AND tc.table_schema = kcu.table_schema " )
                .append(" JOIN information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name " )
                .append(" AND ccu.table_schema = tc.table_schema " )
                .append("  WHERE tc.constraint_type = 'FOREIGN KEY' AND( tc.table_name=? or ccu.table_name=?)");

    List<Object[]> relationShipsForTables= session.createNativeQuery(stringBuffer.toString())
                                                  .setParameter(1,tablename)
                                                  .setParameter(2,tablename)
                                                  .getResultList();
    session.close();
    List<String>tableRelation=new ArrayList<String>();
    for (Object[] r : relationShipsForTables) {
        if(! r[0].toString().equals(tablename))
        tableRelation.add(r[0].toString());
        else
        tableRelation.add(r[2].toString());
    }
    return tableRelation;
}





//function  for column name and foreign column name
public List<RelationShip> findAllTablesRelation(String tablename1, String tablename2)
    {

        EntityManager session = entityManagerFactory.createEntityManager();
        StringBuffer stringBuffer=new StringBuffer();
        stringBuffer.append("SELECT tc.table_name, kcu.column_name, ccu.table_name AS foreign_table_name, " )
                .append(" ccu.column_name AS foreign_column_name FROM information_schema.table_constraints AS tc " )
                .append(" JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name " )
                .append(" AND tc.table_schema = kcu.table_schema JOIN information_schema.constraint_column_usage AS ccu ")
                .append(" ON ccu.constraint_name = tc.constraint_name  AND ccu.table_schema = tc.table_schema " )
                .append(" WHERE tc.constraint_type = 'FOREIGN KEY' AND (tc.table_name=? and ccu.table_name=?)");
        List<Object[]> relation=session.createNativeQuery(stringBuffer.toString())
                              .setParameter(1,tablename1)
                              .setParameter(2,tablename2)
                               .getResultList();

        session.close();
        List<RelationShip>relationShipList=new ArrayList<>();
        for (Object[] r : relation) {
            RelationShip relationShip=new RelationShip();
            relationShip.setTablename(r[0].toString());
            relationShip.setColumnname(r[1].toString());
            relationShip.setTablenameforeign(r[2].toString());
            relationShip.setColumnnameforeign(r[3].toString());
            relationShipList.add(relationShip);
        }
        return relationShipList;
    }
}
