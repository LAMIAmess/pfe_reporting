package com.reporting.Service;
import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;

@Service
public class ServicePdf {

    public ByteArrayInputStream exportPdf(Model model) throws DocumentException, FileNotFoundException {

        @SuppressWarnings("unchecked")
        java.util.List<String> fields = (java.util.List<String>) model.getAttribute("fields");

        @SuppressWarnings("unchecked")
        java.util.List<Object[]> data = (List<Object[]>)model.getAttribute("data");


        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();


        PdfPTable table = new PdfPTable(fields.size());
        table.setWidthPercentage(60);

        Font headFont = FontFactory.getFont(FontFactory.HELVETICA_BOLD);

        PdfPCell hcell;
        for(int i=0;i<fields.size();i++) {
            hcell = new PdfPCell(new Phrase(fields.get(i).toString(), headFont));
            hcell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(hcell);
        }

        for (Object[] r : data)
        {
            if(r !=null)
            {


                for (int k = 0; k < fields.size(); k++) {
                    if (r[k] == null) {
                        r[k] = "";
                    }
                    PdfPCell cell = new PdfPCell(new Phrase(r[k].toString()));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);
                }
            }
        }

        PdfWriter.getInstance(document, new FileOutputStream("hello.pdf"));
        document.open();
        document.add(table);
        document.close();

        return new ByteArrayInputStream(out.toByteArray());

    }
}
