package com.reporting.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import java.sql.Array;
import java.util.List;

@Service
public class ServiceRequetePrepared {
    @Autowired
    private EntityManagerFactory entityManagerFactory;


    public List findResultOfRequete(List<String>field,String table)
    {
        EntityManager session = entityManagerFactory.createEntityManager();
        StringBuffer stringBuffer=new StringBuffer();
        stringBuffer.append("select ");
        for(int i=0;i<field.size();i++)
        {
            if(i==field.size()-1)
            {
                stringBuffer.append(field.get(i)+" ");
            }
            else
            {
                stringBuffer.append(field.get(i)+" , ");
            }
        }
        stringBuffer.append(" from "+table);

        List resultat=session.createNativeQuery(stringBuffer.toString())
                              .getResultList();
        System.out.println("lamia");
        System.out.println();
        session.close();
        return resultat;
    }
}
