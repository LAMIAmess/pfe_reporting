package com.reporting.Service;
import com.opencsv.CSVWriter;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import java.io.FileWriter;
import java.util.List;

@Service
public class ServiceCSV {


    public void exportCSV(Model model) throws Exception {

        //set file name and content type
        @SuppressWarnings("unchecked")
        List<String> fields = (List<String>) model.getAttribute("fields");

        @SuppressWarnings("unchecked")
        List<Object[]>  data = (List<Object[]> )model.getAttribute("data");
      String[] CSV_HEADER = new String[fields.size()];
        String csvFile = "developer.csv";
        FileWriter writer = new FileWriter(csvFile);

        for (int i=0;i<fields.size();i++)
            CSV_HEADER[i]=fields.get(i);
        try (
                CSVWriter csvWriter = new CSVWriter(writer,
                        CSVWriter.DEFAULT_SEPARATOR,
                        CSVWriter.NO_QUOTE_CHARACTER,
                        CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                        CSVWriter.DEFAULT_LINE_END);
        ){
            csvWriter.writeNext(CSV_HEADER);

            for (Object[] r : data) {
                String[] datain = new String[fields.size()];
                if(r !=null)
                {

                    for (int k = 0; k < fields.size(); k++) {
                        if (r[k] == null) {
                            r[k] = "";
                        }
                        datain[k]=r[k].toString();
                    }
                }

                csvWriter.writeNext(datain);
            }

            System.out.println("Write CSV using CSVWriter successfully!");
        }catch (Exception e) {
            System.out.println("Writing CSV error!");
            e.printStackTrace();
        }

    }

}
