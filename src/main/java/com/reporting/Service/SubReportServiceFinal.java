package com.reporting.Service;

import com.reporting.Model.ModelRequete;
import com.reporting.Model.RelationShip;
import com.reporting.Model.ReportModel;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.type.SplitTypeEnum;
import net.sf.jasperreports.engine.xml.JRXmlWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

@Service
public class SubReportServiceFinal {
    JasperDesign jasperDesign;

    @Autowired
    TableQuery tableQuery;


    //initialiser le rapport
    public void loadreport() {
        //creation d'un jasper design pour le rapport
        jasperDesign= new JasperDesign();
        jasperDesign.setName("Reporting");
        jasperDesign.setPageHeight(842);
        jasperDesign.setPageWidth(595);
        jasperDesign.setColumnWidth(555);
        jasperDesign.setLeftMargin(20);
        jasperDesign.setRightMargin(20);
        jasperDesign.setTopMargin(20);
        jasperDesign.setBottomMargin(20);

        //ajouter un background pour le rapport
        JRDesignBand bandBackground = new JRDesignBand();
        bandBackground.setSplitType(SplitTypeEnum.STRETCH);
        jasperDesign.setBackground(bandBackground);

        //ajouter un title pour le rapport
        JRDesignBand bandtitle = new JRDesignBand();
        bandtitle.setHeight(55);
        bandtitle.setSplitType(SplitTypeEnum.STRETCH);
        jasperDesign.setTitle(bandtitle);

        //ajouter une detail pour le rapport
        JRDesignBand bandDetail = new JRDesignBand();
        bandDetail .setHeight(55);
        bandDetail.setSplitType(SplitTypeEnum.STRETCH);
        JRDesignSection designSection = (JRDesignSection) jasperDesign.getDetailSection();
        designSection.addBand(bandDetail);
    }
    //ajouter les parametres au rapport
    public void generateParameters(List<String> parametersList , ArrayList<String> tablename) throws JRException
    {
        for (int i = 0; i < parametersList.size(); i++)
        {
            JRDesignParameter parameter = new JRDesignParameter();
            parameter.setName(parametersList.get(i));
            //retourner le type de parametere
            String type=tableQuery.getTypeOfField(tablename.get(i),parametersList.get(i));
            //ajouter le type au parameter
            this.UpdateParameters(parameter,type);
            jasperDesign.addParameter(parameter);

        }
    }
    //ajouter le type au parametre
    public void UpdateParameters(JRDesignParameter parameter,String type)
    {
        if(type.contains("char")||type.contains("character")||type.contains("name")||type.contains("regclass")||type.contains("text"))
        {
          parameter.setValueClass(String.class);
        }else if (type.contains("bool")||type.contains("boolean"))
        {
             parameter.setValueClass(Boolean.class);
        }
        else if (type.contains("double")||type.contains("real")||type.contains("bigdecimal"))
        {
            parameter.setValueClass(Double.class);
        }
        else if(type.contains("float"))
        {
            parameter.setValueClass(Float.class);
        }
        else if(type.contains("integer")||type.contains("bigint")||type.contains("long")||type.contains("numeric")||type.contains("short")
                ||type.contains("smallint"))
        {
            parameter.setValueClass(Integer.class);
        }
        else if(type.contains("date"))
        {
            parameter.setValueClass(Date.class);
        }
        else if(type.contains("timestamp"))
        {
            parameter.setValueClass(java.sql.Timestamp.class);
        }
        else if (type.contains("array"))
        {
            parameter.setValueClass(List.class);
        }
        else
        {
            parameter.setValueClass(String.class);
        }
    }



    //ajouter les fields au rapport
    public void generateFields(ArrayList<List<String>> fieldsList, ArrayList<String> tablename) throws JRException
    {
        for (int i = 0; i < fieldsList.size(); i++)
        {
            List<String>Fields=fieldsList.get(i);
            for (int j=0;j<Fields.size();j++) {
                JRDesignField field = new JRDesignField();
                field.setName(Fields.get(j));
                String typeField = tableQuery.getTypeOfField(tablename.get(i), Fields.get(j));
                field.getPropertiesMap().setProperty("com.jaspersoft.studio.field.label", Fields.get(j));
                field.getPropertiesMap().setProperty("com.jaspersoft.studio.field.tree.path", tablename.get(i));
                this.UpdateFields(field, typeField);
                jasperDesign.addField(field);
            }

        }

    }

    //ajouter le type au field
    public void UpdateFields(JRDesignField field ,String type)
    {
        if(type.contains("char")||type.contains("character")||type.contains("name")||type.contains("regclass")||type.contains("text"))
        {
            field.setValueClass(String.class);
        }else if (type.contains("bool")||type.contains("boolean"))
        {
            field.setValueClass(Boolean.class);
        }
        else if (type.contains("double")||type.contains("real")||type.contains("bigdecimal"))
        {
            field.setValueClass(Double.class);
        }
        else if(type.contains("float"))
        {
            field.setValueClass(Float.class);
        }
        else if(type.contains("integer")||type.contains("bigint")||type.contains("long")||type.contains("numeric")||type.contains("short")
                ||type.contains("smallint"))
        {
            field.setValueClass(Integer.class);
        }
        else if(type.contains("date"))
        {
            field.setValueClass(Date.class);
        }
        else if(type.contains("timestamp"))
        {
            field.setValueClass(java.sql.Timestamp.class);
        }
        else if (type.contains("array"))
        {
            field.setValueClass(List.class);
        }
        else
        {
            field.setValueClass(String.class);
        }
    }



    public void generateTitle(ArrayList<List<String>> fieldsList)
    {
        JRDesignStaticText  staticText;
        JRDesignBand bndt;
        int row=0;
        for (int i = 0; i < fieldsList.size(); i++)
        {
            List<String>Field=fieldsList.get(i);

            for (int j=0;j<Field.size();j++)
            {
                staticText = new JRDesignStaticText();
                staticText.setX(row++ * 100);
                staticText.setY(20);
                staticText.setWidth(100);
                staticText.setHeight(30);
                staticText.setText(Field.get(j));
                bndt = (JRDesignBand) jasperDesign.getTitle();
                bndt.addElement(staticText);
            }

        }
    }
    public void generateDetails(ArrayList<List<String>> fieldsList)
    {
        JRDesignTextField textField;
        JRDesignExpression expression;
        JRDesignBand bnd;
        int row=0;

        for (int i = 0; i < fieldsList.size(); i++)
        {
            List<String>Field=fieldsList.get(i);
            for (int j=0;j<Field.size();j++)
            {
                textField = new JRDesignTextField();
                textField.setX(row++ * 100);
                textField.setY(10);
                textField.setWidth(100);
                textField.setHeight(30);
                expression = new JRDesignExpression();
                String fieldname = Field.get(j);
                expression.setText("$F{" + fieldname + "}");
                textField.setExpression(expression);
                bnd = (JRDesignBand) jasperDesign.getDetailSection().getBands()[0];
                bnd.addElement(textField);
            }

        }
    }


    public void createSubReport()  {
        try {
            JRXmlWriter.writeReport(jasperDesign, "SUBREP_Lamia.jrxml", "UTF-8");

        } catch (JRException e) {
            System.out.println("ti5dhet");
            e.printStackTrace();
        }
    }


    public void exportReport(String reportFormat) throws FileNotFoundException, JRException, SQLException {
        String path="D:";
        JasperReport jasperReport = null;
        System.out.println("hello from export");
        Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/reis_db", "postgres", "postgresql");
        System.out.println("hello after export");

        File file = ResourceUtils.getFile("SUBREP_Lamia.jrxml");
        try {
          jasperReport=JasperCompileManager.compileReport(file.getAbsolutePath());

        } catch (JRException e) {
            System.out.println("ha rabi ");

            e.printStackTrace();
            System.out.println("hello ");
        }
        Map<String, Object> parameters=new HashMap<String, Object>();
        JasperPrint jasperPrint=JasperFillManager.fillReport(jasperReport,parameters,conn);

        if(reportFormat.equalsIgnoreCase("html"))
        {
            JasperExportManager.exportReportToHtmlFile(jasperPrint,path+"\\employee.html");
        }

        if(reportFormat.equalsIgnoreCase("pdf"))
        {
            JasperExportManager.exportReportToPdfFile(jasperPrint,path+"\\employee.pdf");
        }
        if(reportFormat.equalsIgnoreCase("xls"))
        {
            JasperExportManager.exportReportToXmlFile(jasperPrint,path+"\\employee.xls",false);
        }
    }









    public void raporti(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/reis_db", "postgres", "postgresql");
                    JasperReport jreport = JasperCompileManager.compileReport("SUBREP_Lamia.jrxml");
                    Map<String, Object> parameters=new HashMap<String, Object>();
                    //parameters.put("LastName", "MESSAOUDI");
                    // parameters.put("FirstName", "LAMIA");
                    JasperPrint jprint = JasperFillManager.fillReport(jreport, parameters, conn);
                    JRExporter  exporter= new JRPdfExporter();
                    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,"report.pdf");
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT,jprint);
                    exporter.exportReport();
                    //JasperViewer.viewReport(jprint, false);
                    conn.close();
                }catch (Exception ex){ex.printStackTrace();}
            }
        }).start();
    }




//generate la requete avec la partie select
public StringBuffer generateSqlSelect(ReportModel report)
{
    System.out.println(report.getFieldTable().toString());
    ArrayList<List<String>> fields=new ArrayList<>();
    ArrayList<String> tablesFields=new ArrayList<>();

    List<ModelRequete> tableFielsModels=report.getFieldTable();
    System.out.println("false");
    for (int i=0; i<tableFielsModels.size(); i++)
    {
        fields.add(tableFielsModels.get(i).getFieldsName());
        tablesFields.add(tableFielsModels.get(i).getTableName());
    }
    System.out.println(fields.toString());
    System.out.println(tablesFields.toString());
    StringBuffer requete=new StringBuffer();
     requete.append(" select ");
    for (int i = 0; i < fields.size(); i++) {



            List<String>Fields=fields.get(i);

        for (int j=0;j<Fields.size();j++)
            {

                if (i == tablesFields.size() - 1 && j == Fields.size() - 1) {

                    requete.append(tablesFields.get(i)).append(" .").append(Fields.get(j)).append(" ");
                }
                else {

                    requete.append(tablesFields.get(i)).append(".").append(Fields.get(j)).append(" , ");
                }
            }

    }

    requete.append(" from ");
    for (int i=0;i<tablesFields.size();i++)
    {
        if(i==tablesFields.size()-1)
            requete.append(tablesFields.get(i)).append(" ");
        else
            requete.append(tablesFields.get(i)).append(" , ");
    }

    return requete;
}

//generer la requete avec la partie sql where
public void generateRequeteSqlWhere(ReportModel report,StringBuffer requete,String cond,int i)
{
    try {


        String parameter = tableQuery.getTypeOfField(report.getConditionTable().get(i).getTablename(), report.getConditionTable().get(i).getParameter());
        if (parameter.contains("character") || parameter.contains("text")) {
            System.out.println("test 2");
            if (report.getConditionTable().get(i).getOperateur().equals("between")) {
                System.out.println("test 3");
                requete.append(cond).append(report.getConditionTable().get(i).getTablename()).append(".").append(report.getConditionTable().get(i).getParameter()).append(" ")
                        .append(report.getConditionTable().get(i).getOperateur())
                        .append(" '").append(report.getConditionTable().get(i).getValeur1()).append("' and '").append(report.getConditionTable().get(i).getValeur2())
                        .append("'");

            } else {
                System.out.println("test 4");
                requete.append(cond).append(report.getConditionTable().get(i).getTablename()).append(".")
                        .append(report.getConditionTable().get(i).getParameter()).append(" ").append(report.getConditionTable().get(i).getOperateur())
                        .append(" '").append(report.getConditionTable().get(i).getValeur1()).append("'");
            }

        } else if (parameter.contains("timestamp")) {
            System.out.println("test 5");
            if (report.getConditionTable().get(i).getOperateur().equals("between")) {
                System.out.println("test 6");
                requete.append(cond).append(report.getConditionTable().get(i).getTablename()).append(".")
                        .append(report.getConditionTable().get(i).getParameter()).append(" ").append(report.getConditionTable().get(i).getOperateur())
                        .append(" timestamp '").append(report.getConditionTable().get(i).getValeur1()).append("' and timestamp '")
                        .append(report.getConditionTable().get(i).getValeur2()).append("'");

            } else {
                System.out.println("test 7");
                requete.append(cond).append(report.getConditionTable().get(i).getTablename()).append(".")
                        .append(report.getConditionTable().get(i).getParameter()).append(" ").append(report.getConditionTable().get(i).getOperateur())
                        .append(" timestamp'").append(report.getConditionTable().get(i).getValeur1()).append("'");
            }
        } else {
            System.out.println("test 8");
            if (report.getConditionTable().get(i).getOperateur().equals("between")) {
                System.out.println("test 9");
                requete.append(cond).append(report.getConditionTable().get(i).getTablename()).append(".")
                        .append(report.getConditionTable().get(i).getParameter()).append(" ").append(report.getConditionTable().get(i).getOperateur())
                        .append(" ").append(report.getConditionTable().get(i).getValeur1()).append(" and  ").append(report.getConditionTable().get(i).getValeur2());

            } else {
                System.out.println("test 10");
                requete.append(cond).append(report.getConditionTable().get(i).getTablename()).append(".")
                        .append(report.getConditionTable().get(i).getParameter()).append(" ").append(report.getConditionTable().get(i).getOperateur())
                        .append(" ").append(report.getConditionTable().get(i).getValeur1());
            }
        }


    }catch (Exception e)
    {
        System.out.println("erreur pfffffffffffffff : "+e.getMessage());
    }
}








   //generer la partie sql whereAnd
   public void generateRequeteSqlWhereAnd(ReportModel report, StringBuffer requete)
   {
       for (int i = 1; i < report.getConditionTable().size(); i++)
       {
           this.generateRequeteSqlWhere(report, requete, " and ", i);
       }
   }
    //generer toute la requete
    public StringBuffer generateRequete(ReportModel report) {
        System.out.println("hello lamia hw erreur ");
        StringBuffer requete=this.generateSqlSelect(report);
        System.out.println("res1: "+requete);
        if (report.getConditionTable().size()>0)
        {
            this.generateRequeteSqlWhere(report, requete," where ",0);
        }

        System.out.println("res2: "+requete);
        if(report.getConditionTable().size()>1)
        {
            this.generateRequeteSqlWhereAnd(report,requete);
        }

        System.out.println("res3: "+requete);
        if(report.getJoinTable().size()>0)
        {
            this.generateRequeteJoin(report,requete);
        }
        return requete;
    }

    private void generateRequeteJoin(ReportModel report, StringBuffer requete) {
        List<RelationShip>joins=report.getJoinTable();
        for(int i=0;i<joins.size();i++)
        {
            requete.append(" and ").append(joins.get(i).tablename).append(".").append(joins.get(i).columnname)
                    .append("=").append(joins.get(i).tablenameforeign).append(".").append(joins.get(i).columnnameforeign);
        }
    }

    //ajouter la requete au rapport
    public void generateQuery(String query){
        try {


            JRDesignQuery jdp=new JRDesignQuery();
            jdp.setText(query);
            jdp.setLanguage("SQL");
            jasperDesign.setQuery(jdp);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

    }
}
