package com.reporting.Service;

import com.reporting.Model.Report;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.*;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.type.SplitTypeEnum;
import net.sf.jasperreports.engine.xml.JRXmlWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;

@Service
public class SubReportService {
    JasperDesign jasperDesign;

    @Autowired
    TableQuery tableQuery;


    //initialiser le rapport
    public void loadreport() {
        //creation d'un jasper design pour le rapport
        jasperDesign= new JasperDesign();
        jasperDesign.setName("Reporting");
        jasperDesign.setPageHeight(842);
        jasperDesign.setPageWidth(595);
        jasperDesign.setColumnWidth(555);
        jasperDesign.setLeftMargin(20);
        jasperDesign.setRightMargin(20);
        jasperDesign.setTopMargin(20);
        jasperDesign.setBottomMargin(20);

        //ajouter un background pour le rapport
        JRDesignBand bandBackground = new JRDesignBand();
        bandBackground.setSplitType(SplitTypeEnum.STRETCH);
        jasperDesign.setBackground(bandBackground);

        //ajouter un title pour le rapport
        JRDesignBand bandtitle = new JRDesignBand();
        bandtitle.setHeight(55);
        bandtitle.setSplitType(SplitTypeEnum.STRETCH);
        jasperDesign.setTitle(bandtitle);

        //ajouter une detail pour le rapport
        JRDesignBand bandDetail = new JRDesignBand();
        bandDetail .setHeight(55);
        bandDetail.setSplitType(SplitTypeEnum.STRETCH);
        JRDesignSection designSection = (JRDesignSection) jasperDesign.getDetailSection();
        designSection.addBand(bandDetail);
    }
    //ajouter les parametres au rapport
    public void generateParameters(List<String> parametersList ,String tablename) throws JRException
    {
        for (int i = 0; i < parametersList.size(); i++)
        {
            JRDesignParameter parameter = new JRDesignParameter();
            parameter.setName(parametersList.get(i));
            //retourner le type de parametere
            String type=tableQuery.getTypeOfField(tablename,parametersList.get(i));
            //ajouter le type au parameter
            this.UpdateParameters(parameter,type);
            jasperDesign.addParameter(parameter);

        }
    }
    //ajouter le type au parametre
    public void UpdateParameters(JRDesignParameter parameter,String type)
    {
        if(type.contains("char")||type.contains("character")||type.contains("name")||type.contains("regclass")||type.contains("text"))
        {
          parameter.setValueClass(java.lang.String.class);
        }else if (type.contains("bool")||type.contains("boolean"))
        {
             parameter.setValueClass(java.lang.Boolean.class);
        }
        else if (type.contains("double")||type.contains("real")||type.contains("bigdecimal"))
        {
            parameter.setValueClass(java.lang.Double.class);
        }
        else if(type.contains("float"))
        {
            parameter.setValueClass(java.lang.Float.class);
        }
        else if(type.contains("integer")||type.contains("bigint")||type.contains("long")||type.contains("numeric")||type.contains("short")
                ||type.contains("smallint"))
        {
            parameter.setValueClass(java.lang.Integer.class);
        }
        else if(type.contains("date"))
        {
            parameter.setValueClass(java.util.Date.class);
        }
        else if(type.contains("timestamp"))
        {
            parameter.setValueClass(java.sql.Timestamp.class);
        }
        else if (type.contains("array"))
        {
            parameter.setValueClass(java.util.List.class);
        }
        else
        {
            parameter.setValueClass(java.lang.String.class);
        }
    }



    //ajouter les fields au rapport
    public void generateFields(List<String> fieldsList,String tablename) throws JRException
    {
        for (int i = 0; i < fieldsList.size(); i++)
        {
            JRDesignField field = new JRDesignField();
            field.setName(fieldsList.get(i));
            String typeField=tableQuery.getTypeOfField(tablename,fieldsList.get(i));
            field.getPropertiesMap().setProperty("com.jaspersoft.studio.field.label",fieldsList.get(i));
            field.getPropertiesMap().setProperty("com.jaspersoft.studio.field.tree.path",tablename);
            this.UpdateFields(field,typeField);
            jasperDesign.addField(field);


        }

    }

    //ajouter le type au field
    public void UpdateFields(JRDesignField field ,String type)
    {
        if(type.contains("char")||type.contains("character")||type.contains("name")||type.contains("regclass")||type.contains("text"))
        {
            field.setValueClass(java.lang.String.class);
        }else if (type.contains("bool")||type.contains("boolean"))
        {
            field.setValueClass(java.lang.Boolean.class);
        }
        else if (type.contains("double")||type.contains("real")||type.contains("bigdecimal"))
        {
            field.setValueClass(java.lang.Double.class);
        }
        else if(type.contains("float"))
        {
            field.setValueClass(java.lang.Float.class);
        }
        else if(type.contains("integer")||type.contains("bigint")||type.contains("long")||type.contains("numeric")||type.contains("short")
                ||type.contains("smallint"))
        {
            field.setValueClass(java.lang.Integer.class);
        }
        else if(type.contains("date"))
        {
            field.setValueClass(java.util.Date.class);
        }
        else if(type.contains("timestamp"))
        {
            field.setValueClass(java.sql.Timestamp.class);
        }
        else if (type.contains("array"))
        {
            field.setValueClass(java.util.List.class);
        }
        else
        {
            field.setValueClass(java.lang.String.class);
        }
    }



    public void generateTitle(List<String> fieldsList)
    {
        JRDesignStaticText  staticText;
        JRDesignBand bndt;
        for (int i = 0; i < fieldsList.size(); i++)
        {

            staticText = new JRDesignStaticText();
            staticText.setX(i*100);
            staticText.setY(20);
            staticText.setWidth(100);
            staticText.setHeight(30);
            staticText.setText(fieldsList.get(i));
            bndt= (JRDesignBand) jasperDesign.getTitle();
            bndt.addElement(staticText);

        }
    }
    public void generateDetails(List<String> fieldsList)
    {
        JRDesignTextField textField;
        JRDesignExpression expression;
        JRDesignBand bnd;
        for (int i = 0; i < fieldsList.size(); i++)
        {

            textField = new JRDesignTextField();
            textField.setX(i*100);
            textField.setY(10);
            textField.setWidth(100);
            textField.setHeight(30);
            expression = new JRDesignExpression();
            String fieldname=fieldsList.get(i);
            expression.setText("$F{"+fieldname+"}");
            textField.setExpression(expression);
            bnd= (JRDesignBand) jasperDesign.getDetailSection().getBands()[0];
            bnd.addElement(textField);

        }
    }


    public void createSubReport()  {
        try {
            JRXmlWriter.writeReport(jasperDesign, "SUBREP_Lamia.jrxml", "UTF-8");

        } catch (JRException e) {
            System.out.println("ti5dhet");
            e.printStackTrace();
        }
    }


    public void exportReport(String reportFormat) throws FileNotFoundException, JRException, SQLException {
        String path="D:";
        JasperReport jasperReport = null;
        System.out.println("hello from export");
        Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/reis_db", "postgres", "postgresql");
        System.out.println("hello after export");

        File file = ResourceUtils.getFile("SUBREP_Lamia.jrxml");
        try {
          jasperReport=JasperCompileManager.compileReport(file.getAbsolutePath());

        } catch (JRException e) {
            System.out.println("ha rabi ");

            e.printStackTrace();
            System.out.println("hello ");
        }
        Map<String, Object> parameters=new HashMap<String, Object>();
        JasperPrint jasperPrint=JasperFillManager.fillReport(jasperReport,parameters,conn);

        if(reportFormat.equalsIgnoreCase("html"))
        {
            JasperExportManager.exportReportToHtmlFile(jasperPrint,path+"\\employee.html");
        }

        if(reportFormat.equalsIgnoreCase("pdf"))
        {
            JasperExportManager.exportReportToPdfFile(jasperPrint,path+"\\employee.pdf");
        }
        if(reportFormat.equalsIgnoreCase("xls"))
        {
            JasperExportManager.exportReportToXmlFile(jasperPrint,path+"\\employee.xls",false);
        }
    }









    public void raporti(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Connection conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/reis_db", "postgres", "postgresql");
                    JasperReport jreport = JasperCompileManager.compileReport("SUBREP_Lamia.jrxml");
                    Map<String, Object> parameters=new HashMap<String, Object>();
                    //parameters.put("LastName", "MESSAOUDI");
                    // parameters.put("FirstName", "LAMIA");
                    JasperPrint jprint = JasperFillManager.fillReport(jreport, parameters, conn);
                    JRExporter  exporter= new JRPdfExporter();
                    exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME,"report.pdf");
                    exporter.setParameter(JRExporterParameter.JASPER_PRINT,jprint);
                    exporter.exportReport();
                    //JasperViewer.viewReport(jprint, false);
                    conn.close();
                }catch (Exception ex){ex.printStackTrace();}
            }
        }).start();
    }




//generate la requete avec la partie select
public StringBuffer generateSqlSelect(Report report)
{
    StringBuffer requete=new StringBuffer();
     requete.append(" select ");
    for (int i = 0; i < report.getFields().size(); i++) {
        if (i!=report.getFields().size()-1)
            requete.append(report.getFields().get(i)).append(" , ");
        else
           requete.append(report.getFields().get(i)).append(" ");

    }
    requete.append("   from  ").append(report.getTable());
    return requete;
}
//generer la requete avec la partie sql where
public void generateRequeteSqlWhere(Report report,StringBuffer requete,String cond,int i)
{
    try {


        String parameter = tableQuery.getTypeOfField(report.getTable(), report.getParameters().get(i).getParameter());
        if (parameter.contains("character") || parameter.contains("text")) {
            System.out.println("test 2");
            if (report.getParameters().get(i).getOperateur().equals("between")) {
                System.out.println("test 3");
                requete.append(cond).append(report.getParameters().get(i).getParameter()).append(" ").append(report.getParameters().get(i).getOperateur())
                        .append(" '").append(report.getParameters().get(i).getValeur1()).append("' and '").append(report.getParameters().get(i).getValeur2())
                        .append("'");

            } else {
                System.out.println("test 4");
                requete.append(cond).append(report.getParameters().get(i).getParameter()).append(" ").append(report.getParameters().get(i).getOperateur())
                        .append(" '").append(report.getParameters().get(i).getValeur1()).append("'");
            }

        } else if (parameter.contains("timestamp")) {
            System.out.println("test 5");
            if (report.getParameters().get(i).getOperateur().equals("between")) {
                System.out.println("test 6");
                requete.append(cond).append(report.getParameters().get(i).getParameter()).append(" ").append(report.getParameters().get(i).getOperateur())
                        .append(" timestamp '").append(report.getParameters().get(i).getValeur1()).append("' and timestamp '")
                        .append(report.getParameters().get(i).getValeur2()).append("'");

            } else {
                System.out.println("test 7");
                requete.append(cond).append(report.getParameters().get(i).getParameter()).append(" ").append(report.getParameters().get(i).getOperateur())
                        .append(" timestamp'").append(report.getParameters().get(i).getValeur1()).append("'");
            }
        } else {
            System.out.println("test 8");
            if (report.getParameters().get(i).getOperateur().equals("between")) {
                System.out.println("test 9");
                requete.append(cond).append(report.getParameters().get(i).getParameter()).append(" ").append(report.getParameters().get(i).getOperateur())
                        .append(" ").append(report.getParameters().get(i).getValeur1()).append(" and  ").append(report.getParameters().get(i).getValeur2());

            } else {
                System.out.println("test 10");
                requete.append(cond).append(report.getParameters().get(i).getParameter()).append(" ").append(report.getParameters().get(i).getOperateur())
                        .append(" ").append(report.getParameters().get(i).getValeur1());
            }
        }


    }catch (Exception e)
    {
        System.out.println("erreur pfffffffffffffff : "+e.getMessage());
    }
}








   //generer la partie sql whereAnd
   public void generateRequeteSqlWhereAnd(Report report,StringBuffer requete)
   {
       for (int i = 1; i < report.getParameters().size(); i++)
       {
           this.generateRequeteSqlWhere(report, requete, " and ", i);
       }
   }
    //generer toute la requete
    public StringBuffer generateRequete(Report report) {
        StringBuffer requete=this.generateSqlSelect(report);
        System.out.println("res1: "+requete);
        if (report.getParameters().size()>0)
        {
            this.generateRequeteSqlWhere(report, requete," where ",0);
        }

        System.out.println("res2: "+requete);
        if(report.getParameters().size()>1)
        {
            this.generateRequeteSqlWhereAnd(report,requete);
        }

        System.out.println("res3: "+requete);

        return requete;
    }
    //ajouter la requete au rapport
    public void generateQuery(String query){
        try {


            JRDesignQuery jdp=new JRDesignQuery();
            jdp.setText(query);
            jdp.setLanguage("SQL");
            jasperDesign.setQuery(jdp);
        }catch (Exception e)
        {
            System.out.println(e.getMessage());
        }

    }
}
