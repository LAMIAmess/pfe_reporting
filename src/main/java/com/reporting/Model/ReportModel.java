package com.reporting.Model;

import lombok.Data;

import java.util.List;
@Data
public class ReportModel {
    public List<ModelRequete> fieldTable;
    public List<RelationShip> joinTable;
    public List<ParameterModel> conditionTable;

}
