package com.reporting.Model;

import lombok.Data;

@Data
public class ParameterModel {
    String tablename;
    String parameter;
    String operateur;
    String valeur1;
    String valeur2;
}
