package com.reporting.Model;

import lombok.Data;

import java.util.List;

@Data
public class Report {
    public List<ParameterModel> parameters;
    public List<String> fields;
    public String table;

}
