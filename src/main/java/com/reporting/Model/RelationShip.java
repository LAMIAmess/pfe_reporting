package com.reporting.Model;

import lombok.Data;

import javax.persistence.Entity;

@Data
public class RelationShip {
    public String tablename;
    public String tablenameforeign;
    public String columnname;
    public String columnnameforeign;
}
