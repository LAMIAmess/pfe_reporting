package com.reporting.Controller;

import com.reporting.Model.ModelRequete;
import com.reporting.Service.*;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.core.io.InputStreamResource;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.util.List;

@RestController
@RequestMapping(value = "/Preparedrequete")
@CrossOrigin(origins = "http://localhost:4200")
public class RequetePreparedController {
    @Autowired
    private ServiceRequetePrepared serviceRequetePrepared;
    @Autowired
    private ServiceXls serviceXls;
    @Autowired
    private ServiceCSV serviceCSV;
    @Autowired
    private ServicePdf servicePdf;


    @PostMapping("/getList")
    public List getListResultRequete(@RequestBody ModelRequete modelRequete) {
        return serviceRequetePrepared.findResultOfRequete(modelRequete.getFieldsName(), modelRequete.getTableName());
    }

    @PostMapping("/generate")
    public void generatedExcel(@RequestBody ModelRequete modelRequete, Model model) throws Exception {

        model.addAttribute("fields", modelRequete.getFieldsName());
        model.addAttribute("data", serviceRequetePrepared.findResultOfRequete(modelRequete.getFieldsName(), modelRequete.getTableName()));
        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file
        serviceXls.createxls(model, workbook);

    }

    @PostMapping("/generatecsv")
    public void generatedcsv(@RequestBody ModelRequete modelRequete, Model model, HttpServletResponse response) throws Exception {
        // response.setContentType("text/csv");
        //response.setHeader("Content-Disposition", "attachment; file=customers.csv");
        model.addAttribute("fields", modelRequete.getFieldsName());
        model.addAttribute("data", serviceRequetePrepared.findResultOfRequete(modelRequete.getFieldsName(), modelRequete.getTableName()));
        serviceCSV.exportCSV(model);
        System.out.println("fin");
    }

    @PostMapping("/generatepdf")
    public ResponseEntity<InputStreamResource> generatedpdf(@RequestBody ModelRequete modelRequete, Model model) throws Exception {

        model.addAttribute("fields", modelRequete.getFieldsName());
        model.addAttribute("data", serviceRequetePrepared.findResultOfRequete(modelRequete.getFieldsName(), modelRequete.getTableName()));
        ByteArrayInputStream bis = servicePdf.exportPdf(model);

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=customers.pdf");
        System.out.println("fin");

        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
}
