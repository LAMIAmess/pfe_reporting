package com.reporting.Controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.reporting.Model.RelationShip;
import com.reporting.Service.RelationShipQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/relationForeignKey")
@CrossOrigin(origins = "http://localhost:4200")
public class RelationShipQueryContoller {
    @Autowired
    RelationShipQuery relationShipQuery;

    @GetMapping("/getAllRelationShips/{tablename}")
    public List<RelationShip> getAllRelationShips(@PathVariable("tablename") String tablename) throws JsonProcessingException {
        return relationShipQuery.findRelationShip(tablename);
    }
    @GetMapping("/getTablesRelationShips/{tablename}")
    public List<String> getTablesRelationShips(@PathVariable("tablename") String tablename) throws JsonProcessingException {
        return relationShipQuery.findTableRelation(tablename);
    }

    @GetMapping("/getAllTablesRelation/{tablename}")
    public List<String> getAllTablesRelation(@PathVariable("tablename") String tablename) throws JsonProcessingException {
        return relationShipQuery.findAllTablesRelation(tablename);
    }
    @GetMapping("/getAllRelation/{tablename1}/{tablename2}")
    public List<RelationShip> getAllRelations(@PathVariable("tablename1") String tablename1,@PathVariable("tablename2") String tablename2)
    {
        List<RelationShip>relationIntermediare = new ArrayList<>();
        List<RelationShip>relation=new ArrayList<>();
         relation=relationShipQuery.findAllTablesRelation(tablename1,tablename2);
         relationIntermediare=relationShipQuery.findAllTablesRelation(tablename2,tablename1);
         for (int i=0;i<relationIntermediare.size();i++)
         {
             RelationShip relationShip=new RelationShip();
             relationShip.setTablename(relationIntermediare.get(i).getTablenameforeign());
             relationShip.setColumnname(relationIntermediare.get(i).getColumnnameforeign());
             relationShip.setTablenameforeign(relationIntermediare.get(i).getTablename());
             relationShip.setColumnnameforeign(relationIntermediare.get(i).getColumnname());
             relation.add(relationShip);
         }
         return relation;

    }

}
