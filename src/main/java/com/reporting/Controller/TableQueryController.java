package com.reporting.Controller;

import com.reporting.Service.TableQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/requete")
@CrossOrigin(origins = "http://localhost:4200")
public class TableQueryController {
    @Autowired
    private TableQuery tableQuery;


    @GetMapping("/getAllTables")
    public List<String> getAllTables()
    {
        System.out.println("textTable");
        return tableQuery.getAllTables();
    }


    @GetMapping("/getAllFields/{tablename}")
    public List<String>getAllFields(@PathVariable("tablename") String tablename)
    {
        return tableQuery.getAllFields(tablename);
    }


    @GetMapping("/getTypeOfField/{tablename}/{fieldname}")
    public String getTypeOfField(@PathVariable("tablename") String tablename,@PathVariable("fieldname")String fieldname)
    {

        return tableQuery.getTypeOfField(tablename,fieldname);
    }



}
