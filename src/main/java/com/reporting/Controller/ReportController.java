package com.reporting.Controller;

import com.reporting.Model.ParameterModel;
import com.reporting.Model.Report;
import com.reporting.Service.SubReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/report")
@CrossOrigin(origins = "http://localhost:4200")
public class ReportController {

    @Autowired
    private SubReportService subReportService;
    private Report report;

    @PostMapping("/generate")
    public void generate(@RequestBody Report report)
    {
        ArrayList<String> paramerters=new ArrayList<>();
        List<ParameterModel> parameterModels=report.getParameters();
        for (int i=0; i<parameterModels.size(); i++)
        {
            paramerters.add(parameterModels.get(i).getParameter());
        }
        System.out.println("debut");


        try {
            subReportService.loadreport();
            System.out.println("load report");


            String requete=subReportService.generateRequete(report).toString();
            System.out.println("requette ailleurs :"+requete);
            subReportService.generateQuery(requete);
            System.out.println("generate requete");

            System.out.println("debut generate parameters");
            subReportService.generateParameters(paramerters,report.getTable());
            System.out.println("generate parametrs");




            System.out.println(" debut generate field");
            subReportService.generateFields(report.getFields(),report.getTable());
            System.out.println("generate field");



            subReportService.generateTitle(report.getFields());
            System.out.println("generate title");

            subReportService.generateDetails(report.getFields());
            System.out.println("generate details ");

            subReportService.createSubReport();
            System.out.println("createSubReport ");

            subReportService.exportReport("pdf");
            System.out.println("export Report ");

           // subReportService.raporti();
            //System.out.println("rapporti  ");

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }


}
