package com.reporting.Controller;

import com.reporting.Model.ModelRequete;
import com.reporting.Model.ParameterModel;
import com.reporting.Model.Report;
import com.reporting.Model.ReportModel;
import com.reporting.Service.SubReportService;
import com.reporting.Service.SubReportServiceFinal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/reportF")
@CrossOrigin(origins = "http://localhost:4200")
public class ReportControllerFinal {

    @Autowired
    private SubReportServiceFinal subReportServiceFinal;
    private Report report;

    @PostMapping("/generate")
    public void generate(@RequestBody ReportModel report)
    {
        System.out.println(report.toString());
        ArrayList<String> paramerters=new ArrayList<>();
        ArrayList<String> tables=new ArrayList<>();

        List<ParameterModel> parameterModels=report.getConditionTable();
        for (int i=0; i<parameterModels.size(); i++)
        {
            paramerters.add(parameterModels.get(i).getParameter());
            tables.add(parameterModels.get(i).getTablename());
        }
        System.out.println("debut");


        try {
            subReportServiceFinal.loadreport();
            System.out.println("load report");


            String requete=subReportServiceFinal.generateRequete(report).toString();
            System.out.println("requette ailleurs :"+requete);
            subReportServiceFinal.generateQuery(requete);
            System.out.println("generate requete");

            System.out.println("debut generate parameters");
            subReportServiceFinal.generateParameters(paramerters,tables);
            System.out.println("generate parametrs");


            ArrayList<List<String>> fields=new ArrayList<>();
            ArrayList<String> tablesFields=new ArrayList<>();

            List<ModelRequete> tableFielsModels=report.getFieldTable();
            for (int i=0; i<tableFielsModels.size(); i++)
            {
                fields.add(tableFielsModels.get(i).getFieldsName());
                tablesFields.add(tableFielsModels.get(i).getTableName());
            }

            System.out.println(" debut generate field");
            subReportServiceFinal.generateFields(fields,tablesFields);
            System.out.println("generate field");



            subReportServiceFinal.generateTitle(fields);
            System.out.println("generate title");

            subReportServiceFinal.generateDetails(fields);
            System.out.println("generate details ");

            subReportServiceFinal.createSubReport();
            System.out.println("createSubReport ");

            subReportServiceFinal.exportReport("pdf");
            System.out.println("export Report ");

           // subReportService.raporti();
            //System.out.println("rapporti  ");

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }


}
